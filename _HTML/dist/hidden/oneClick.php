
<div class="mfiModal zoomAnim popup-style max-width-425">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Заказать в один клик</div>
	</div>
	<div class="popup-style_main">
		<div class="popup-style_desc"><span>Введите номер телефона. Наш менеджер свяжется с Вами для оформления заказа.</span></div>
		<div data-form="true" class="wForm wFormDef wFormPreloader">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput phone-mask" required="required" type="tel" name="demo_callback" id="demo_callback" placeholder="Ваш номер телефона" data-rule-phoneUA="true"/>
					<div for="demo_callback" class="inpInfo">Телефон *</div>
				</div>
			</div>
			<input type="hidden" value="" name="id_item"/>
			<div class="wFormRow w_last">
				<button class="wSubmit gradient-button">Отправить</button>
			</div>
		</div>
	</div>
</div>