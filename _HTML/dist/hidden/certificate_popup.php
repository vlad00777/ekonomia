
<div class="mfiModal zoomAnim popup-style max-width-425 certificate-popup">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Ввод номера сертификата</div>
	</div>
	<div class="popup-style_main">
		<div class="popup-style_desc"><span>Введите номер сертификата и получите скидку на заказ.</span></div>
		<div data-form="true" data-not-ajax="true" class="wForm wFormDef">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="text" name="promocode" id="promocode" placeholder="Номер сертификата"/>
					<div for="promocode" class="inpInfo">Промокод *</div>
				</div>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit gradient-button">Применить</button>
			</div>
		</div>
	</div>
</div>