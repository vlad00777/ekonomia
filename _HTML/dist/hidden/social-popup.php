
<div class="mfiModal zoomAnim popup-style max-width-425 social_links_popup">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Редактирование соц. сети</div>
	</div>
	<div class="popup-style_main">
		<div class="popup-style_desc"><span>Введите ссылку на свой профиль в социальной сети.</span></div>
		<div data-form="true" data-ajax="wForm-demo-submit.php" class="wForm wFormDef">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="url" name="demo_url_input" id="demo_url_input" placeholder="Ссылка" data-rule-url="true"/>
					<div class="inpInfo">Ссылка</div>
				</div>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit gradient-button">Отправить</button>
			</div>
		</div>
	</div>
</div>