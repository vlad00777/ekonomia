
<div class="mfiModal zoomAnim popup-style max-width-425">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Заказать обратный звонок</div>
	</div>
	<div class="popup-style_main">
		<div class="popup-style_desc"><span>Оставьте Ваши контактные данные и наши менеджеры свяжутся с Вами</span></div>
		<div data-form="true" data-ajax="wForm-demo-submit.php" class="wForm wFormDef wFormPreloader">
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput" required="required" type="text" name="demo_word_input" id="demo_word_input" placeholder="Ваше имя" data-rule-word="true"/>
					<div class="inpInfo">Ваше имя *</div>
				</div>
			</div>
			<div class="wFormRow">
				<div class="wFormInput">
					<input class="wInput phone-mask" required="required" type="tel" name="demo_callback" id="demo_callback" placeholder="Ваш номер телефона" data-rule-phoneUA="true"/>
					<div for="demo_callback" class="inpInfo">Телефон *</div>
				</div>
			</div>
			<div class="wFormRow w_last">
				<button class="wSubmit gradient-button">Подтверждение</button>
			</div>
		</div>
	</div>
</div>