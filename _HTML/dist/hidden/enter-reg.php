
<div id="enterReg" class="enterRegPopup zoomAnim popup-style">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Вход в интернет-магазин ekonomia.com.ua</div>
	</div>
	<div class="popup-style_main">
		<div class="enterReg_top">
			<div class="popupBlock enterBlock wCur">
				<div class="erTitle">Вход</div>
				<div class="popupContent">
					<div id="entrForm" data-form="true" data-ajax="wForm-demo-submit.php" class="enterBlock_form visForm wForm wFormDef">
						<div class="wFormRow">
							<input type="email" name="enter_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="Ваш e-mail*" required=""/>
							<div class="inpInfo">E-mail</div>
						</div>
						<div class="wFormRow">
							<input type="password" name="enter_pass" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Ваш пароль*" required=""/>
							<div class="inpInfo">Пароль</div>
						</div>
						<div class="height-style">
							<label class="checkBlock">
								<input type="checkbox" checked="checked"/><ins></ins><span>Запомнить данные</span>
							</label>
							<div id="forget_pass" class="passLink call-popupLink">Забыли пароль?</div>
						</div>
						<div class="tar">
							<button class="wSubmit gradient-button">Войти</button>
						</div>
					</div>
					<div id="forgetForm" data-form="true" data-ajax="wForm-demo-submit.php" class="enterBlock_form wForm wFormDef">
						<div class="wFormRow">
							<input type="email" name="forget_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="E-mail*" required=""/>
							<div class="inpInfo">E-mail</div>
						</div>
						<div class="forgetInf">После отправления, в течении 5 минут к Вам на почту придут инструкции по восстановлению пароля.</div>
						<div id="remember_pass" class="passLink call-popupLink">Вернуться</div>
						<div class="tar">
							<button class="wSubmit gradient-button">Отправить</button>
						</div>
					</div>
				</div>
			</div>
			<div data-form="true" data-ajax="wForm-demo-submit.php" class="popupBlock regBlock wForm wFormDef">
				<div class="erTitle">Регистрация</div>
				<div class="popupContent">
					<div class="wFormRow">
						<input type="email" name="reg_email" data-msg-required="Это поле необходимо заполнить" data-msg-email="Пожалуйста, введите корректный Email" placeholder="Ваш e-mail*" required=""/>
						<div class="inpInfo">E-mail</div>
					</div>
					<div class="wFormRow">
						<input type="password" name="reg_pass" data-msg-required="Это поле необходимо заполнить" data-msg-minlength="Пожалуйста, введите не меньше 4 символов" data-rule-minlength="4" placeholder="Ваш пароль*" required=""/>
						<div class="inpInfo">Пароль</div>
					</div>
					<label class="checkBlock">
						<input type="checkbox" name="reg_agree" data-msg-required="Это поле нужно отметить" required=""/><ins></ins><span>Я согласен с условиями использования и обработку моих персональных данных*</span>
					</label>
					<div class="tar">
						<button class="wSubmit gradient-button">Зарегистрироваться</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="popupBlock socEnter">
		<div class="erTitle">Вход через социальные сети</div>
		<div class="popupContent socLinkEnter">
			<button title="Вконтакте" class="eVk"></button>
			<button title="Одноклассники" class="eOd"></button>
			<button title="Facebook" class="eFb"></button>
			<button title="Twitter" class="eTw"></button>
			<button title="Google +" class="eGp"></button>
		</div>
	</div>
</div>