
<div class="mfiModal zoomAnim popup-style max-width-1005">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Мой отзыв о SAMSUNG WW70J5217IW/UA</div>
	</div>
	<div class="rating-popup">
		<div class="rating-popup_set">
			<div class="rating-popup_set_text">Оцените товар</div>
			<div class="rating-popup_set_stars">
				<div class="rating"><span class="rating__stars big">
						<input type="radio" name="rating"/><i>
							<svg>
								<use xlink:href="#big_star"></use>
							</svg><span>отличный</span></i>
						<input type="radio" name="rating"/><i>
							<svg>
								<use xlink:href="#big_star"></use>
							</svg><span>хороший</span></i>
						<input type="radio" name="rating" checked=""/><i>
							<svg>
								<use xlink:href="#big_star"></use>
							</svg><span>нормальный</span></i>
						<input type="radio" name="rating"/><i>
							<svg>
								<use xlink:href="#big_star"></use>
							</svg><span>так себе</span></i>
						<input type="radio" name="rating"/><i>
							<svg>
								<use xlink:href="#big_star"></use>
							</svg><span>плохой</span></i></span></div>
			</div>
		</div>
		<div class="rating-popup_desc">
			<p>При написании отзыва постарайтесь ответить на следующие вопросы:</p>
			<ol>
				<li>Как долго вы пользуетесь этим товаром?</li>
				<li>По каким критериям вы выбирали товар?</li>
				<li>Назовите достоинства и недостатки товара. Рекомендуете ли вы его другим?</li>
			</ol>
		</div>
		<div class="rating-popup_form">
			<div data-form="true" class="wForm wFormDef wFormPreloader">
				<div class="wFormRow">
					<label for="demo_word_input" class="wLabel">Ваше имя</label>
					<div class="wCaption">(для отображения на сайте)<i>*</i></div>
					<div class="wFormInput">
						<input class="wInput" required="required" type="text" name="demo_word_input" id="demo_word_input" placeholder="" data-rule-word="true"/>
						<div class="inpInfo">Ваше имя</div>
					</div>
				</div>
				<div class="wFormRow">
					<label for="demo_email_input" class="wLabel">Ваше e-mail</label>
					<div class="wCaption">(на сайте не публикуется)<i>*</i></div>
					<div class="wFormInput">
						<input class="wInput" required="required" type="email" name="demo_email_input" id="demo_email_input" placeholder="" data-rule-email="true"/>
						<div class="inpInfo">Ваше e-mail</div>
					</div>
				</div>
				<div class="wFormRow">
					<label for="demo_required_input" class="wLabel">Достоинства</label>
					<div class="wFormInput">
						<input class="wInput" type="text" name="demo_required_input" id="demo_required_input" placeholder=""/>
						<div class="inpInfo">Достоинства</div>
					</div>
				</div>
				<div class="wFormRow">
					<label for="demo_required_input2" class="wLabel">Недостатки</label>
					<div class="wFormInput">
						<input class="wInput" type="text" name="demo_required_input2" id="demo_required_input2" placeholder=""/>
						<div class="inpInfo">Недостатки</div>
					</div>
				</div>
				<div class="wFormRow">
					<label for="demo_minlength_input" class="wLabel">Комментарий</label>
					<div class="wFormTextarea">
						<textarea class="wTextarea" required="required" type="text" name="demo_minlength_input" id="demo_minlength_input" placeholder="" data-rule-minlength='20'></textarea>
						<div class="inpInfo">Комментарий</div>
					</div>
				</div>
				<div class="wFormRow w_last">
					<button class="wSubmit gradient-button">Оставить свой отзыв</button>
				</div>
			</div>
		</div>
	</div>
</div>