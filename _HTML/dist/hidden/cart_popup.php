
<div id="basket" class="mfiModal zoomAnim popup-style max-width-965">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title small">Ваш заказ в магазине “ekonomia.com.ua”</div>
	</div>
	<div class="popup-cart magnific">
		<div class="popup-cart_item w_clearfix">
			<div class="popup-cart_item_img"><a href="#"><img src="images/wash-item.jpg"/></a></div>
			<div class="popup-cart_item_cener">
				<div class="popup-cart_item_title"><a href="#">LED-телевизор SAMSUNG UE32J5500AUXUA</a></div>
				<div class="popup-cart_item_countPrice">1 шт. х&nbsp;<i>343</i>грн.</div>
				<div class="popup-cart_item_pricetel"><span>34</span>&nbsp;грн</div>
			</div>
			<div class="popup-cart_item_price"><span>343</span>&nbsp;грн</div>
			<div class="popup-cart_item_count">
				<div data-id="" data-delta="-1" class="popup-cart_item_dec js-change-count"></div>
				<div class="popup-cart_item_input">
					<input type="tel" value="1" maxlength="2" onkeypress="return event.charCode &gt;= 48 &amp;&amp; event.charCode &lt;= 57" class="js-count"/>
				</div>
				<div data-id="" data-delta="1" class="popup-cart_item_inc js-change-count"></div>
				<div class="popup-cart_item_remove"><span data-id="" class="call-popupLink js-remove-from-cart">удалить товар</span></div>
			</div>
		</div>
	</div>
	<div class="footer-popup-basket w_clearfix">
		<div class="w_fll"><span class="call-popupLink">Купить еще что-нибудь в магазине</span></div>
		<div class="w_flr">
			<div class="footer-popup-basket_total"><span>Итого:</span><b><span>0</span>&nbsp;грн</b></div><a href="/_HTML/dist/step2.html" class="gradient-button">Оформить</a>
		</div>
	</div>
</div>