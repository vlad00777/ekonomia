
<div class="mfiModal zoomAnim popup-style max-width-425">
	<div class="popup-style_header">
		<div class="popup-style_logo"><span class="svgHolder">
				<svg>
					<use xlink:href="#logo"/>
				</svg></span></div>
		<div class="popup-style_title">Сообщить о наличии</div>
	</div>
	<div class="popup-style_main">
		<div class="popup-style_desc"><span>Наш менеджер свяжется с Вами, когда товар появится к наличии.</span></div>
	</div>
</div>