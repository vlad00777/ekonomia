jQuery(document).ready(function($) {

    function chgTxt(el) {
        var tmp = el.text();
        el.text(el.data('txt')).data('txt', tmp);
    }

    function checkMisMatch() {
        $('.compareTable').find('.mismath').removeClass('mismath');
        $('.compareTable').find('tr').each(function(j,el){
            if (j > 0) {
                var val = '';
                for (var i = 1; i < el.children.length; i++) {
                    var td = el.children[i].innerHTML;
                    if (i === 1) {
                        val = td;
                    } else if (!!i && val != td) {
                        $(el.children[i]).addClass('mismath');
                    }
                }
            }
        });
        checkCompareWidth();
    }

    function removeMatch(){
        if($('.compareSection').hasClass('compareMismath')){
            var count = 0;
            $('.compareTable tr').each(function(index,el){
                if (index > 0) {
                    $(el).find('td').each(function(i,element){
                        if (!$(element).hasClass('mismath')) {
                            count++;
                        }
                    });
                    if (count == $(el).find('td').length){
                        $(el).hide();
                    }
                    count = 0;
                }
            });
        } else{
            $('.compareTable tr').show();
        }
    }

    function checkCompareWidth() {
        var sw = $('.compareHold').width();
        var tw = $('.compareTable').width();
        var wrp = $('.compareWrapp');
        if (tw >= sw) {
            wrp.addClass('compareInset');
        } else {
            wrp.removeClass('compareInset');
        }
    }

    checkMisMatch();

    $('.compareTable').on('mouseover', 'td', function() {
        var td = $(this);
        var tr = td.parent('tr');
        var tb = td.closest('table');
        if (td.index() > 0) {
            tb.find('.highlight').removeClass('highlight');
            tb.find('tr td:nth-child('+(td.index()+1)+')').addClass('highlight');
        }
    }).on('mouseleave', function() {
        $(this).find('.highlight').removeClass('highlight');
    }).on('click', '.js-remove-item', function(){
        var td = $(this).closest('td');
        if (td.siblings().length <= 2) {
            $('.compareSection').addClass('compareEmpty');
            $('.compareTable').empty();
        } else {
            $('.compareTable').find('tr td:nth-child('+(td.index()+1)+')').remove();
            checkMisMatch();
        }
    });

    $('.toggleMisMath').on('click', function(){
        $('.toggleMisMath').toggleClass('is-active');
        $('.compareSection').toggleClass('compareMismath');
        removeMatch();

    });
    $('.js-clear-compare').on('click', function() {
        $('.compareSection').addClass('compareEmpty');
        $('.compareTable').empty();
    });

    $(window).load(function(event) {
        checkCompareWidth();
    });

    $(window).resize(function(event) {
        checkCompareWidth();
    });

});