<?php
    if(isset($_POST['category']) && isset($_POST['count'])) {
        $category  = $_POST['category'];
        $count = $_POST['count'];

        $result = array();

        for($i = 0; $i<10; $i++){
            $result[$i]['title'] = 'some news some news';
            $result[$i]['id'] = $i;
            $result[$i]['price'] = $i*11;
            $result[$i]['category'] = 'Бытовая техника';
            if($i %3 == 0) {
                $result[$i]['promotions'] = 'new';
                $result[$i]['promotions_text'] = 'Новинка';
            }
            if($i %2 == 0) {
                $result[$i]['promotions'] = 'promo';
                $result[$i]['promotions_text'] = 'Акция';
            }
            if($i %4 == 0) {
                $result[$i]['promotions'] = 'hit';
                $result[$i]['promotions_text'] = 'Хит продаж';
            }
            $result[$i]['img'] = 'images/news-img.jpg';
        }

        echo json_encode($result);
    }
 ?>