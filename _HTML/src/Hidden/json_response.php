<?php
    if(isset($_POST['offset']) && isset($_POST['count'])) {
        $offset  = $_POST['offset'];
        $count = $_POST['count'];

        $result = array();

        for($i = 0; $i<$count; $i++){
            $result[$i]['rating'] = '4';
            $result[$i]['name'] = 'new name';
            $result[$i]['minuses'] = 'Минусы';
            $result[$i]['pluses'] = 'Плюсы';
            $result[$i]['text'] = 'Хорошая стиральная машина. Отлично стирает. Барабан по окончании всегда "встает" дверцами вверх, что удобно при открытии.';
            $result[$i]['date'] = '25 июня 2016';
            $result[$i]['plus'] = '5';
            $result[$i]['minus'] = '1';
        }

        echo json_encode($result);
    }
 ?>